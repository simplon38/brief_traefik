package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

func main() {
	clientID := os.Getenv("CLIENT_ID")
	clientSecret := os.Getenv("CLIENT_SECRET")
	redirectURL := os.Getenv("REDIRECT_URI")
	scopes := []string{"openid", "email"}
	port := os.Getenv("PORT")

	config := &oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		RedirectURL:  redirectURL,
		Scopes:       scopes,
		Endpoint:     google.Endpoint,
	}

	http.HandleFunc("/_oauth", func(w http.ResponseWriter, r *http.Request) {
		// Générer le lien d'authentification
		authURL := config.AuthCodeURL("state", oauth2.AccessTypeOnline)

		http.Redirect(w, r, authURL, http.StatusSeeOther)
	})

	http.HandleFunc("/oauth2callback", func(w http.ResponseWriter, r *http.Request) {
		// Récupérer le code d'autorisation à partir des paramètres de la requête
		code := r.URL.Query().Get("code")
		if code == "" {
			log.Fatal("Code d'autorisation manquant dans la requête.")
		}

		// Échanger le code d'autorisation contre un jeton d'accès
		token, err := config.Exchange(context.Background(), code)
		if err != nil {
			log.Fatalf("Échec de l'échange du code d'autorisation : %v", err)
		}

		// Vérifier si l'authentification a réussi
		if token.Valid() {
            cookie := &http.Cookie{
                Name:  "auth_token",
                Value: token.AccessToken,
            // Définir d'autres options de cookie si nécessaire
            }
        
            // Ajouter le cookie à la réponse
            http.SetCookie(w, cookie)

			http.Redirect(w, r, "/", http.StatusSeeOther)
		} else {
			http.Redirect(w, r, "/failure", http.StatusSeeOther)
		}

	})

	http.HandleFunc("/checkauth", func(w http.ResponseWriter, r *http.Request) {
        cookie, err := r.Cookie("auth_token")
        if err != nil || cookie.Value == "" {
            http.Redirect(w, r, "/_oauth", http.StatusSeeOther)
            return
        }

        // Créer un jeton d'accès à partir du cookie
        token := &oauth2.Token{
            AccessToken: cookie.Value,
        }

        // Vérifier si le jeton d'accès est valide
        if token.Valid() {
            w.WriteHeader(http.StatusOK)
            fmt.Fprintf(w, "Utilisateur connecté !")
            return
        } else {
            http.Redirect(w, r, "/_oauth", http.StatusSeeOther)
        }

	})
    http.HandleFunc("/success", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Authentification réussie !")
	})

	http.HandleFunc("/failure", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Échec de l'authentification.")
	})

	log.Fatal(http.ListenAndServe(":"+port, nil))
}
