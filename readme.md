ajouter dans le deploy traefik:
   - '--providers.kubernetescrd.allowCrossNamespace=true'
   - '--providers.kubernetescrd.namespaces=traefik,prod'

test:
kubectl get all --all-namespaces -o wide | grep  10.224.0.6         
patch le forwardip:
kubectl patch svc traefik -p '{"spec":{"externalTrafficPolicy":"Local"}}' -n traefik



mkdir ca
# Génération d'une clé privée
openssl genrsa -aes256 -out ca/ca.key 4096
# Génération du certificat racine CA
openssl req -new -x509 -sha256 -days 1095 -key ca/ca.key -out ca/ca.crt

mkdir client
openssl genrsa -out client/client.key 4096
# Génération du Certificate Signing Request (CSR)
openssl req -new -key client/client.key -sha256 -out client/client.csr
# Génération du certficat du client
openssl x509 -req -days 365 -sha256 -in client/client.csr -CA ca/ca.crt -CAkey ca/ca.key -set_serial 1 -out client/client.crt
# Génération du .p12
openssl pkcs12 -export -clcerts -in client/client.crt -inkey client/client.key -out client/client.p12

# Vérification
# Pour le CRT
openssl verify -CAfile ca/ca.crt client/client.crt
# Pour le .p12
openssl pkcs12 -in client/client.p12 -noout -info
